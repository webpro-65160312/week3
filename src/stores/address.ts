import { ref } from 'vue'
import { defineStore } from 'pinia'
type AddressBook = {
  id: number
  name: string
  gender: string
  tel: string
}

export const useAddressStore = defineStore('address', () => {
  let lastID = 0

  const visibleForm = ref(false)

  const address = ref<AddressBook>({
    id: 0,
    name: '',
    gender: 'Male',
    tel: ''
  })

  const addressBooks = ref<AddressBook[]>([])

  const save = () => {
    if (address.value.id > 0) {
      // edit
      const editedIndex = addressBooks.value.findIndex((item) => item.id === address.value.id)
      addressBooks.value[editedIndex] = address.value
    } else {
      lastID++
      addressBooks.value.push({ ...address.value, id: lastID })
    }
    resetItem()
  }

  const edit = (id: number) => {
    const editedIndex = addressBooks.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressBooks.value[editedIndex]))
  }

  const remove = (id: number) => {
    const deletedIndex = addressBooks.value.findIndex((item) => item.id === id)
    addressBooks.value.splice(deletedIndex, 1)
  }

  const resetItem = () => {
    address.value = {
      id: 0,
      name: '',
      gender: 'Male',
      tel: ''
    }
  }

  const toggleForm = () => {
    visibleForm.value = !visibleForm.value
  }

  return { address, addressBooks, save, edit, remove, toggleForm, visibleForm }
})
